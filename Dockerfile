FROM alpine:3.18

RUN apk add --no-cache git build-base cmake pkgconf gettext-dev bzip2-dev curl-dev libnl3-dev rtmpdump-dev libidn-dev ncurses-dev freetype-dev mesa-dev gmp-dev irrlicht-dev libjpeg-turbo-dev jsoncpp-dev leveldb-dev luajit-dev lua5.1-dev libogg-dev openal-soft-dev libpng-dev postgresql-dev hiredis-dev sqlite-dev libvorbis-dev libxi-dev zlib-dev doxygen libxrandr-dev libx11-dev zstd-dev openssl-dev samurai
WORKDIR /usr/src
RUN cd /usr/src && \
	git clone --depth=1 -b minenux https://gitlab.com/minenux/minetest-engine-multicraft2 multicraft && \
	cd multicraft && \
	git clone --depth=1 -b stable-5.2 https://codeberg.org/minenux/minetest-game-minetest ./games/minetest && \
	rm -fr ./games/minetest/.git
WORKDIR /usr/src/multicraft
RUN mkdir build && \
	cd build && \
	cmake .. \
		-DCMAKE_INSTALL_PREFIX=/usr/ -DBUILD_SERVER=ON -DBUILD_CLIENT=OFF -DRUN_IN_PLACE=OFF -DENABLE_CURL=ON -DENABLE_SOUND=ON -DENABLE_LUAJIT=ON -DENABLE_GETTEXT=ON -DENABLE_FREETYPE=ON -DENABLE_SYSTEM_GMP=ON -DENABLE_SYSTEM_JSONCPP=ON -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_BUILD_TYPE=Release -DENABLE_POSTGRESQL=ON \
		-DRUN_IN_PLACE=OFF -DCUSTOM_BINDIR=/usr/games -DCUSTOM_LOCALEDIR=/usr/share/locale -DCUSTOM_SHAREDIR=/usr/share/games/multicraft -DCUSTOM_EXAMPLE_CONF_DIR=/etc/multicraft .. \
	make && \
	make install

FROM alpine:3.18

RUN apk add --no-cache libstdc++ libgcc  bzip2 curl libnl3 librtmp libidn libintl libncursesw freetype mesa-gl gmp irrlicht libjpeg-turbo jsoncpp leveldb luajit lua5.1 libogg openal-soft-libs libpng libpq hiredis sqlite-libs libvorbis libxi zlib-dev libxrandr libx11 zstd && \
	mkdir -p /var/games/multicraft && adduser -D multicraft --uid 30000 -h /var/games/multicraft && chown -R multicraft:multicraft /var/games/multicraft
COPY --from=0 /usr/share/games/multicraft /usr/share/games/multicraft
COPY --from=0 /usr/games/multicraftserver /usr/games/multicraftserver
COPY --from=0 /etc/multicraft/multicraft.conf /etc/multicraft/multicraft.conf
USER multicraft:multicraft
EXPOSE 40000/udp 40000/tcp
WORKDIR /var/games/multicraft
CMD ["/usr/games/multicraftserver", "--port", "40000", "--config", "/etc/multicraft/multicraft.conf", "--gameid", "minetest", "--world", "minetest"]
