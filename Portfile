# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# por file for MAC ports.. currently only tested in older versions of MACOS(X) 

PortSystem              1.0
PortGroup               github 1.0
PortGroup               cmake 1.1

compiler.cxx_standard   2011
compiler.thread_local_storage   yes

github.setup            multicraft multicraft 2.0.6
revision                1

# the game version could be different from the multicraft version
set game_version        2.0.6

# to add on more files to a github portgroup download
# have to cache the preset distfiles from the github PG
set main_distfile       ${distfiles}

# then add another distfile, with a direct URL as can't use the github PG again
# set game_distfile       ${game_version}${extract.suffix}
# set game_mastersite     https://codeberg.org/minenux/minetest-game-minetest/archive/

distfiles               ${main_distfile}:main

master_sites            ${github.master_sites}:main

checksums               ${main_distfile} \
                        rmd160  550fd4c6767cce4bb5dd79f9d907b233e21b9b2c \
                        sha256  8a320b3355381ba7ed4b6a41bfb0f39cb2858eb7bae0f6fc5935b329a53bde56 \
                        size    10116355

# rename directory - from github portgroup
post-extract {
    if {[file exists [glob -nocomplain ${workpath}/${github.author}-${github.project}-*]] && \
        [file isdirectory [glob -nocomplain ${workpath}/${github.author}-${github.project}-*]]} {
        move [glob ${workpath}/${github.author}-${github.project}-*] ${workpath}/${distname}
    }
}

license                 LGPL-2.1+
categories              games
platforms               darwin
maintainers             @mckaygerhard openmaintainer
description             Survival Craft Build Sandbox Game STOLEN from MINETEST
long_description        ${description} - the multipprotocol version enhanched for play in older or newer servers

homepage                https://www.minetest.org

depends_build-append    path:bin/doxygen:doxygen \
                        port:mesa

depends_lib-append      port:irrlichtmt \
                        path:include/turbojpeg.h:libjpeg-turbo \
                        port:libogg \
                        port:libvorbis \
                        port:freetype \
                        port:gettext \
                        port:leveldb \
                        port:sqlite3 \
                        port:zstd \
                        path:lib/libluajit-5.1.2.dylib:luajit \
                        port:gmp \
                        port:curl \
                        port:jsoncpp \
                        port:spatialindex \
                        port:xorg-libX11 \
                        port:xorg-libXxf86vm

# see https://trac.macports.org/ticket/58315 - possibly fixable?
universal_variant       no
supported_archs         x86_64 x86

# 001. the original build calls fixup_bundle to move all the deps into the app bundle.
#    this doesn't work correctly with macports destrooting, and isn't necessary for a macports install so deleted it
# patchfiles-append       001-patch-src-CMakeLists-disable-bundlefixup.diff

configure.args-append   -DCMAKE_BUILD_TYPE=Release \
	-DBUILD_SERVER=ON -DBUILD_CLIENT=ON \
	-DENABLE_CURL=ON \
	-DENABLE_SOUND=ON \
	-DENABLE_LUAJIT=ON \
	-DENABLE_GETTEXT=ON \
	-DENABLE_FREETYPE=ON \
	-DENABLE_SYSTEM_GMP=ON \
	-DENABLE_SYSTEM_JSONCPP=ON \
	-DCMAKE_VERBOSE_MAKEFILE=ON \
	-DCMAKE_BUILD_TYPE=Release \
	-DENABLE_POSTGRESQL=ON \
	-DENABLE_REDIS=OFF -DCURSES_INCLUDE_PATH:FILEPATH=${prefix}/include \
	-DCMAKE_INSTALL_PREFIX:PATH=${applications_dir} \
	-DBUILD_UNITTESTS=OFF \
	-DENABLE_UPDATE_CHECKER=OFF \
	-DENABLE_SPATIAL=ON \
	-DINSTALL_DEVTEST=ON \
	-DLUA_INCLUDE_DIR:PATH=${prefix}/include/luajit-2.1 \
	-DLUA_LIBRARY:FILEPATH=${prefix}/lib/libluajit-5.1.dylib \
	-DVERSION_EXTRA=MacPorts-rev${revision}

post-destroot {
    move ${workpath}/multicraft_game-${game_version}   ${destroot}${applications_dir}/multicraft.app/Contents/Resources/games/multicraft_game
}
