MultiCraft
==========

[![pipeline status](https://gitlab.com/minenux/minetest-engine-multicraft2/badges/master/pipeline.svg)](https://gitlab.com/minenux/minetest-engine-multicraft2/-/commits/master)
[![License](https://img.shields.io/badge/license-LGPLv3.0%2B-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/license-CC_BY--SA_4.0-orange.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

MultiCraft Open Source is a Survival Craft Build Sandbox Game STOLEN from MINETEST where a niche of guys make money from the work of others!

..cos is based on the Minetest project, which is developed by a [number of contributors](https://github.com/minetest/minetest/graphs/contributors) but doe snot show in the Apple Store or Gplay Store!

## Information
--------------

IF you wish downloable artifacts to direct usage go to:

* Linux: https://software.opensuse.org//download.html?project=home%3Avenenux%3Aminenux&package=minetest [![build and download multicraft](https://build.opensuse.org/projects/home:venenux:minenux/packages/minetest/badge.svg?type=percent)](https://build.opensuse.org/package/show/home:venenux:minenux/minetest)
* Windos: we do not support it but you can download modified versions from : https://t.me/latam_minecraft_chat/766185 access maybe need some rules!
* MAc:  we do not support it but you can download modified versions from : https://t.me/latam_minecraft_chat/766185 access maybe need some rules!
* Androit:  we do not support it but you can download modified versions from : https://t.me/latam_minecraft_chat/766185 access maybe need some rules!

This is the repository for sources of multicraft, The truth is that the sources only compile well in Linux, for other cases there are strong dependencies on the configurations made by the team that replaces the work of minetest.

This means that it will only compile the last commit and it will be **impossible to reproduce the compilation in previous commits or versions even if you have the dependencies, since the configurations and the reason for them are obfuscated!**

## Technical info
-----------------

This source does not remove backguar compatibility.. you can connect or serve older or newer minetest protocols!

Visit wiki.minetest.org